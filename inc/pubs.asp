<%
 '	/**
 '	* pubs.asp 30a7cf7f70c50a1b437d946a6e493a5b
 '	* Created by yichaxin.com V190304_090410
 '	* @author yujianyue<admin@ewuyi.net>
 '	* Date: 190304@5c7c79948e6ce http://12391.net
 '	* 注意;著作权所有,不得公开发布和出售
 '	*/
on error resume next
duan=tiaojian1
mdbtype=moexe '只能是xls格式文件哦不要修改
if len(tiaojian1)=2 then
qianmian1=left(tiaojian1,1)&"&nbsp;&nbsp;"&right(tiaojian1,1)
else
qianmian1=tiaojian1
end if
Function IsFile(FilePath)
Set Fso=Server.CreateObject("Scri"&"pting.File"&"Sys"&"temObject")
If (Fso.FileExists(Server.MapPath(FilePath))) Then
IsFile=True
Else
IsFile=False
End If
Set Fso=Nothing
End Function
mimafile="../inc/mima.dat"
descfile="../inc/desc.dat"
tima = time()
Function tram(Strs)
if instr(Strs,vbtab)>0 then Strs=replace(Strs,vbtab," ")
if instr(Strs,vbcrlf)>0 then Strs=replace(Strs,vbcrlf,"<br>")
Strs = Trim(Strs)
if len(Strs)>0 then
tram = Strs
else
tram = "&nbsp;"
end if
End Function
'==============================
'函 数 名：FsoFileRead
'作    用：读取文件
'参    数：文件相对路径FilePath
'==============================
Function FsoFileRead(FilePath,charset)
Set objAdoStream = Server.CreateObject("A"&"dod"&"b.St"&"r"&"eam")
objAdoStream.Type=2
objAdoStream.mode=3
objAdoStream.charset=charset
objAdoStream.open
objAdoStream.LoadFromFile Server.MapPath(FilePath)
FsoFileRead=objAdoStream.ReadText
objAdoStream.Close
Set objAdoStream=Nothing
End Function
Function filemima(mimafile)
Set fso = CreateObject("Scripting.FileSystemObject")
Set fd = fso.OpenTextFile(server.MapPath(mimafile), 1, True)
if fd.AtEndOfStream=false then
contentd = fd.readline()
end if
filemima=trim(contentd)
fd.close
end Function
Function filekey(texts)
filekey=0
rekey="-/-\-%-@-.-"
keyes=split(rekey,"-")
nnnnn=Ubound(keyes)
For m=1 To Ubound(keyes)-1
rekeys=keyes(m)
rekeys=trim(rekeys)
if instr(texts,rekeys)>0 and len(rekeys)>0 then
filekey=filekey+1
end if
next
End Function
'定义获取排序文件列表的函数
Function getSortedFiles(folderPath)
Dim rs, fso, folder, File
Const adInteger = 3
Const adDate = 7
Const adVarChar = 200
Set rs = Server.CreateObject("ADODB.Recordset")
Set fso = Server.CreateObject("Scripting.FileSystemObject")
Set folder = fso.GetFolder(folderPath)
Set fso = Nothing
With rs.Fields
.Append "Name", adVarChar, 200
.Append "Type", adVarChar, 200
.Append "DateCreated", adDate
.Append "DateLastAccessed", adDate
.Append "DateLastModified", adDate
.Append "Size", adInteger
.Append "TotalFileCount", adInteger
End With
rs.Open
For Each File In folder.Files
rs.AddNew
rs("Name") = File.Name
rs("Type") = File.Type
rs("DateCreated") = File.DateCreated
rs("DateLastAccessed") = File.DateLastAccessed
rs("DateLastModified") = File.DateLastModified
rs("Size") = File.Size
rs.Update
Next
'设置排序规则：按名称排序
rs.Sort = "DateLastModified DESC"
'设置排序规则：依次按文件大小倒序，按修改日期倒序
'rs.Sort = "Size DESC, DateLastModified DESC"
rs.MoveFirst
Set folder = Nothing
Set getSortedFiles = rs
End Function
'==============================
'函 数 名： AlertUrl(AlertStr,Url)
'作 用：警告后转入指定页面
'==============================
Function AlertUrl(AlertStr,Url)
Response.Write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"" />" &vbcrlf
Response.Write "<script>" &vbcrlf
Response.Write "alert('"&AlertStr&"');" &vbcrlf
Response.Write "location.href='"&Url&"';" &vbcrlf
Response.Write "</script>" &vbcrlf
Response.End()
End Function
'==============================
'函 数 名： AlertBack(AlertStr)
'作 用：警告后返回上一页面
'==============================
Function AlertBack(AlertStr)
Response.Write "<meta http-equiv=""Content-Type"" content=""text/html; charset=gb2312"" />" &vbcrlf
Response.Write "<script>" &vbcrlf
Response.Write "alert('"&AlertStr&"');" &vbcrlf
Response.Write "history.go(-1)" &vbcrlf
Response.Write "</script>"&vbcrlf
Response.End()
End Function

 'xls1doo:pubs.asp:190304@5c7c79948e6ce 
%>